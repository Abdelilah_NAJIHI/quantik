<?php


namespace Quantik;

require_once ("../src/PlateauQuantik.php");

<?php

class ActionQuantik
{
    protected PlateauQuantik $plateau;

    public function __construct(PlateauQuantik $plateau) {
        $this->plateau = $plateau;
    }
    
     public function getPlateau(): PlateauQuantik
    {
        return $this->plateau;
    }

    public function isRowWin(int $rowNum): bool {
        return self::isComboWin($this->plateau->getRow($rowNum));
    }

    public function isColWin(int $colNum): bool {
        return self::isComboWin($this->plateau->getCol($colNum));
    }

    public function isCornerWin(int $dir): bool {
        return self::isComboWin($this->plateau->getCorner($dir));
    }

    public function isValidePose(int $rowNum, int $colNum, PieceQuantik $piece): bool {
        $res = true;
        //Vérif sur la ligne
        $res &= self::isPieceValide($this->plateau->getRow($rowNum), $piece);

        //Vérif sur la colonne
        $res &= self::isPieceValide($this->plateau->getCol($colNum), $piece);

        //Vérif sur le coin
        $cornerPieces = $this->plateau->getCorner($this->plateau->getCornerFromCoord($rowNum, $colNum));
        $res &= self::isPieceValide($cornerPieces, $piece);

        return $res;
    }

  

    private static function isComboWin(array $pieces): bool {
        $arrFormes = array();
        foreach($pieces as $plateau) {
            array_push($arrFormes, $plateau->getForme());
        }

        //On retourne vrai s'il y a bien 4 valeurs uniques dans les formes (donc, 4 formes différentes)
        return count(array_unique($arrFormes)) == 4;
    }

    private static function isPieceValide(array $pieces, PieceQuantik $p): bool {
        foreach($pieces as $ptemp) {
            //Même forme, couleur différente : pose invalide.
            if ($ptemp->getForme() == $p->getForme() && $ptemp->getCouleur() != $p->getCouleur())
                return false;
        }

        return true;
    }
}