<?php

namespace Quantik;
require_once "ActionQuantik.php";
use PHPUnit\Framework\TestCase;

class ActionQuantikTest extends TestCase
{

    public function testIsRowWin()
    {
        $p = new PlateauQuantik();
        $p->setPiece(0,0,PieceQuantik::initWhiteCube());
        $p->setPiece(0,1,PieceQuantik::initWhiteSphere());
        $p->setPiece(0,2,PieceQuantik::initBlackCylindre());
        $p->setPiece(0,3,PieceQuantik::initWhiteCone());

        $a = new ActionQuantik($p);

        self::assertTrue($a->isRowWin(0));
        self::assertTrue($a->isRowWin(1));

        $p->setPiece(2,2,PieceQuantik::initWhiteCylindre());
        $p->setPiece(2,3,PieceQuantik::initWhiteCone());
        self::assertFalse($a->isRowWin(2));




    }

    public function testIsValidePose():void{
        $p = new PlateauQuantik();
        $a = new ActionQuantik($p);
        $p->setPiece(0,0,PieceQuantik::initWhiteSphere());
        $p->setPiece(0,1,PieceQuantik::initWhiteCone());
        $p->setPiece(0,1,PieceQuantik::initBlackCube());

        self::assertTrue($a->isValidePose(0,3,PieceQuantik::initWhiteCylindre()));
        self::assertFalse($a->isValidePose(3,2,PieceQuantik::initWhiteCube()));

        $p->setPiece(1,2,PieceQuantik::initWhiteCylindre());
        self::assertFalse($a->isValidePose(0,3,PieceQuantik::initWhiteCylindre()));

    }

}
