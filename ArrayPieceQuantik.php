<?php
require_once 'PieceQuantik.php';

class ArrayPieceQuantik{

	protected array $piecesQuantik;
	protected $taille;



public function __construct() {
    $this->taille = $taille;
    $this->piecesQuantiks = array();
  }
  
    public function getPieceQuantik(int $pos): PieceQuantik {
        return $this->piecesQuantik[$pos];
    }

    public function setPieceQuantik(int $pos, PieceQuantik $piece): void {
        $this->piecesQuantik[$pos] = $piece;
    }

    public function addPieceQuantik(PieceQuantik $piece): void {
        array_push($this->piecesQuantik, $piece);
        $this->taille++;
    }


public function removePieceQuantik(int $pos){
	if($this->taille != 0 && $pos < $this->taille){
		array_splice($this->piecesQuantik, $pos, null);
		$this->taille-=1;
	}
}
public function getTaille() : int {
    return $this->taille;
  }

 public function __toString() : string {
    $resultat = "[";
    for($j=0;$j<$this->getTaille();$j++) {
      $c = $this->getPieceQuantik($j);
      $resultat .= $c->__toString();
    }
    return $resultat .= "]";
  }



public static function initPiecesNoires() : ArrayPieceQuantik {
    
        $piecesNoires = new ArrayPieceQuantik();
        $cuw=PieceQuantik::initBlackCube();
        $cuw2=PieceQuantik::initBlackCube();
        $cow=PieceQuantik::initBlackCone();
        $cow2=PieceQuantik::initBlackCone();
        $sph=PieceQuantik::initBlackSphere();
        $sph2=PieceQuantik::initBlackSphere();
        $cyl=PieceQuantik::initBlackCylindre();
        $cyl2=PieceQuantik::initBlackCylindre();

        $piecesNoires->addPieceQuantik($cuw);
        $piecesNoires->addPieceQuantik($cuw2);
        $piecesNoires->addPieceQuantik($cow);
        $piecesNoires->addPieceQuantik($cow2);
        $piecesNoires->addPieceQuantik($sph);
        $piecesNoires->addPieceQuantik($sph2);
        $piecesNoires->addPieceQuantik($cyl);
        $piecesNoires->addPieceQuantik($cyl2);
        return $piecesNoires;
    }
    
    public static function initPiecesBlanches() : ArrayPieceQuantik {
    
        $piecesBlanches = new ArrayPieceQuantik();
        $cuw=PieceQuantik::initWhiteCube();
        $cuw2=PieceQuantik::initWhiteCube();
        $cow=PieceQuantik::initWhiteCone();
        $cow2=PieceQuantik::initWhiteCone();
        $sph=PieceQuantik::initWhiteSphere();
        $sph2=PieceQuantik::initWhiteSphere();
        $cyl=PieceQuantik::initWhiteCylindre();
        $cyl2=PieceQuantik::initWhiteCylindre();

        $piecesBlanches->addPieceQuantik($cuw);
        $piecesBlanches->addPieceQuantik($cuw2);
        $piecesBlanches->addPieceQuantik($cow);
        $piecesBlanches->addPieceQuantik($cow2);
        $piecesBlanches->addPieceQuantik($sph);
        $piecesBlanches->addPieceQuantik($sph2);
        $piecesBlanches->addPieceQuantik($cyl);
        $piecesBlanches->addPieceQuantik($cyl2);
        return $piecesBlanches;
    }

}
?>