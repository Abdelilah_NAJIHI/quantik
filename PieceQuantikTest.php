<?php

namespace Quantik;
require_once ("PieceQuantik.php");
//use TestCase;

class PieceQuantikTest 
{
    public function test__toString()
    {
        $v = PieceQuantik::initVoid();
        self::assertEquals("[VOID]",$v->__toString());

        $p = PieceQuantik::initWhiteCone();
        self::assertEquals("[CO,W]",$p->__toString());
    }

    public function testInitWhiteCone()
    {
        $p = PieceQuantik::initWhiteCone();
        $this->assertInstanceOf(
            PieceQuantik::class,
            $p
        );
        self::assertEquals(PieceQuantik::CONE,$p->getForme());
        self::assertEquals(PieceQuantik::WHITE,$p->getCouleur());
    }
    
    echo"$p";
//print_r($s);


}


?>