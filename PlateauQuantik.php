<?php
require_once("PieceQuantik.php");

class PlateauQuantik{
	public const NBROWS=4;
	public const NBCOLS=4;
	public const NW=0;
	public const NE=1;
	public const SW=2;
	public const SE=3;
	protected array $cases =[];

	public function __construct() {
		for($i=0;$i<$this->NBROWS;$i++){
			for($j=0;$j<$this->NBCOLS;$j++){
				$cases[$i][$j]=PieceQuantik::initVoid();
			}
		}
}
  	public function getPiece(int $rowNUM,int $colNUM) : PieceQuantik {
      self::checkBounds($rowNUM,$colNUM);
    	 return $this->cases[$rowNum][$colNum];

  }
   public static function checkBounds(int $rowNUM,int $colNUM): void
   {
      if($rowNUM<0 or $rowNUM >= self::NBROWS or $colNUM<0 or $colNUM >= self::NBCOLS){
        throw new Exception("Error Processing Request", 1);
        
      }
   }

  	public function setPiece(int $rowNUM,int $colNUM,PieceQuantik $p) {
       self::checkBounds($rowNUM,$colNUM);
  		 $this -> $cases[$rowNum][$colNum] = $p;
}

   public function getRow(int $numROW): array{
   	if($numROW>=$this->NBROWS){
   		return null;
   	}
   	$resultat=[];
   	for($i=0;i<$this->NBCOLS;$i++){
   		$resultat[$i] =$this->$cases[$numROW][$i];
			}
			return $resultat;
   	}


   	public function getCol(int $numcol): array{
  		/* if($numcol>=$this->NBCOLS){
  		 	return null;
  		 }*/

  		 $resultat=[];
  		 for($i=0;$i<$this->NBROWS;$i++){
  		 	$resultat[$i]=this->$cases[$i][$numcol];
  		 }
  		 $return $resultat;
	}


   	public function getCorner(int $dir): array{
   		$resultat=[];

   		switch($dir){
   			case(self::NW){
   				$DeI=0;
   				$DeJ=0;
   				$AI=self::NBCOLS/2;
   				$AJ=self::NBROWS/2;
   			} 
              break;
   			case(self::NE){
   				$DeI=0;
   				$DeJ=0;
   				$AI=self::NBCOLS/2;
   				$AJ=self::NBROWS/4;
   			}
              break;
   			case(self::SW){
   				$DeI=0;
   				$DeJ=0;
   				$AI=self::NBCOLS/4;
   				$AJ=self::NBROWS/2;
   			}
              break;
   			case(self::SE){
   				$DeI=0;
   				$DeJ=0;
   				$AI=self::NBCOLS;
   				$AJ=self::NBROWS;
   			}	
              break;
   		}
   		for($i=$DeI,$i<$AI;$i++){
   			for($j=$DeJ;$j<$AJ;$j++){
   				$resultat[$i][$j]=this->$cases[$AI][$AJ];
   			}
   		}
   		return $resultat;
	}
   	public function __toString() : string {
   		$resultat="";
   		for($i=0;$i<$this->NBROWS;$i++){
   			for($j=0;$j<$this->NBCOLS;$j++){
   				$resultat="[";
   				$resultat=$this->cases[$i][$j];
   				$resultat="]";

   			}
   			$resultat.=("\n");
   		}
   		 return(string) $resultat;  		
  }

  public static getCornerFromCoord(int $rowNUM,int $colNUM):int{
   public static function getCornerFromCoord($rowNum, $colNum) : int {
        if($rowNum < 2 && $colNum < 2){
            return self::NW;
        } else if($rowNum > 1 && $colNum < 2){
            return self::NE;
        } else if($rowNum < 2 && $colNum > 1){
            return self::SW;
        } else if($rowNum > 1 && $colNum > 1){
            return self::SE;
        }
    }
  }
 }

?>