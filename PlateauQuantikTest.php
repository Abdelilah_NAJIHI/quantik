<?php
require_one("PlateauQuantik.php");

class PlateauQuantikTest extends TestCase{

	public function testSetPiece(){
		$p=new PlateauQuantik();
		$p->setPiece(2,2,PieceQuantik::initWhiteCylindre());
		self::assertEquals(PieceQuantik::initWhiteCylindre(),$p->getPiece(2,2));
	}
	public function test__construct(){
		$p=new PlateauQuantik();
		self::assertEquals(PieceQuantik::initVoid(),$p->getPiece(0,0));
	}

	

	public function testGetCorner():void{
		$p=new PlateauQuantik();
		$p->setPiece(0,0,PieceQuantik::initWhiteCylindre());
		$p->setPiece(0,1,PieceQuantik::initWhiteCylindre());
		$p->setPiece(1,0,PieceQuantik::initWhiteCylindre());
		$p->setPiece(1,1,PieceQuantik::initWhiteCylindre());
		$corner=$p->getCorner(PlateauQuantik::NW);
		self::assertEquals(PieceQuantik::initWhiteCylindre(),$corner[0]);
		self::assertEquals(PieceQuantik::initWhiteCylindre(),$corner[1]);
		self::assertEquals(PieceQuantik::initWhiteCylindre(),$corner[2]);
		self::assertEquals(PieceQuantik::initWhiteCylindre(),$corner[3]);

	}

}
?>