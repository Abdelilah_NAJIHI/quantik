<?php

require_once "PlateauQuantik.php";
require_once "ArrayPieceQuantik.php";

/**
 * Class QuantikUIGenerator
 */
class QuantikUIGenerator
{

    /**
     * @param string $title
     * @return string
     */
    public static function getDebutHTML(string $title = "Jeu Quantik"): string
    {
        return "<!doctype html>
<html lang='fr'>
    <head>
        <meta charset='UTF-8'>
        <title>$title</title>
        <link rel=\"stylesheet\" type=\"text/css\" href=\"quantik.css\" />
    </head>
    <body>
        <h1 id='titre'>$title</h1>
        <div class='quantik'>\n";
    }

    /**
     * @return string
     */
    public static function getFinHTML(): string
    {
        return "</div></body>\n</html>";
    }

    /**
     * @param string $message
     * @return string
     */
    public static function getPageErreur(string $message): string
    {
        header("HTTP/1.1 400 Bad Request");

        $resultat = self::getDebutHTML("400 Bad Request");
        $resultat .= "<h2>$message</h2>";
        $resultat .= "<p><br /><br /><br /><a href='{$_SERVER['SCRIPT_NAME']}?reset'>Retour à l'accueil...</a></p>";
        $resultat .= self::getFinHTML();

        return $resultat;
    }

    /**
     * @param PieceQuantik $pq
     * @return string
     */
    public static function getButtonClass(PieceQuantik $pq):string {
        if ($pq->getForme()==PieceQuantik::VOID)
            return "vide";
        $ch = $pq->__toString();
        return substr($ch,1,2).substr($ch,4,1);
    }

    /**
     * production d'un bloc HTML pour présenter l'état du plateau de jeu,
     * l'attribution de classes en vue d'une utilisation avec les est un bon investissement...
     * @param PlateauQuantik $p
     * @return string
     */
    public static function getDivPlateauQuantik(PlateauQuantik $p): string
    {
        $resultat = "<div><table class='tablePlateau'>\n";

        for ($i = 0; $i < $p::NBROWS; $i++) {
            $resultat .= "<tr>\n";
            for ($j = 0; $j < $p::NBCOLS; $j++) {
                $resultat .= "<td class='tdPlateau'><button class='forme".
                    $p->getPiece($i, $j)->getForme().
                    $p->getPiece($i, $j)->getCouleur().
                    " btnDisabled' disabled></button></td>\n";
            }
            $resultat.= "</tr>\n";
        }

        $resultat.= "</table>\n</div>\n";
        return $resultat;
    }

    /**
     * @param ArrayPieceQuantik $apq
     * @param int $pos permet d'identifier la pièce qui a été sélectionnée par l'utilisateur avant de la poser (si != -1)
     * @return string
     */
    public static function getDivPiecesDisponibles(ArrayPieceQuantik $apq, int $pos = -1): string {
        $resultat = "<div><table>";

        for ($i = 0; $i < 2; $i++) {
            $resultat .= "<tr>";
            for ($j = 0; $j < $apq->getTaille() / 2; $j++) {
                $idx = ($i * $apq->getTaille() / 2) + $j;
                $resultat .= "<td  class='tdPiece".($idx == $pos ? ",tdselected":"")."' >";
                $resultat .= "<button disabled class='forme".
                    $apq->getPiece($idx)->getForme().
                    $apq->getPiece($idx)->getCouleur().
                    " btnDisabled'></button></td>";
            }
            $resultat .= "</tr>";
        }

        $resultat .= "</table></div>";

        return $resultat;
    }

    /**
     * @param ArrayPieceQuantik $apq
     * @return string
     */
    public static function getFormSelectionPiece(ArrayPieceQuantik $apq): string {
        $resultat = "<form action=\"\" method=\"get\"><table>";

        for ($i = 0; $i < 2; $i++) {
            $resultat .= "<tr>";
            for ($j = 0; $j < $apq->getTaille() / 2; $j++) {
                $idx = ($i * $apq->getTaille() / 2) + $j;
                $resultat .= "<td class='tdPiece'>";
                $resultat .= ($idx < $apq->getTaille() ? self::getBoutonPieceChoix($apq->getPiece($idx), $idx) : "") . "</input>";
                $resultat .= "</td>";
            }
            $resultat .= "</tr>";
        }

        $resultat .= "<input name='action' value='choisirPiece' hidden>";
        $resultat .= "</table></form>";

        return $resultat;
    }

    /**
     * @param PlateauQuantik $plateau
     * @param PieceQuantik $piece
     * @param int $position position de la pièce qui sera posée en vue de la transmettre via un champ caché du formulaire
     * @return string
     */
    public static function getFormPlateauQuantik(PlateauQuantik $plateau, PieceQuantik $piece, int $position): string {
        $action = new ActionQuantik($plateau);
        $ret = "<form action='{$_SERVER['SCRIPT_NAME']}'>";
        $ret .= "<table class='tablePlateau'>";

        for ($i = 0; $i < PlateauQuantik::NBROWS; $i++) {
            $ret .= "<tr>";
            for ($j = 0; $j < PlateauQuantik::NBCOLS; $j++) {
                $class = "tdPlateau";
                if ($action->isValidePose($i, $j, $piece)) $class .= " valid";
                $ret .= "<td class='$class'>".self::getBoutonPlateau($action, $plateau, $piece, $i, $j)."</td>";
            }
            $ret .= "</tr>";
        }

        $ret .= "</table>";
        $ret .= "<input name='position' value='$position' hidden>";
        $ret .= "<input name='action' value='poserPiece' hidden>";
        $ret .= "</form>";
        return $ret;
    }

    /**
     * @return string
     */
    public static function getFormBoutonAnnuler() : string {
        $resultat  = "<div class='annuler'>";
        $resultat .= "<form action='{$_SERVER['SCRIPT_NAME']}'>";
        $resultat .= "<input class =\"annuler\" type=\"submit\" name = \"annuler\" value=\"annuler\">";
        $resultat .= "<input name='action' value='annulerChoix' hidden>";
        $resultat .= "</form>";
        $resultat .= "</div>";
        return $resultat;

    }

    public static function getBoutonPieceChoix(PieceQuantik $p, int $i): string {
        $dis = "";
        $class = "forme".$p->getForme()."".$p->getCouleur();
        if ($p->getForme() == PieceQuantik::VOID) {
            $dis = "disabled";
            $class .= " btnDisabled";
        } else $class .= " btnCase";
        return "<button class='$class' type='submit' name='position' value='$i' $dis></button>";
    }

    public static function getBoutonPlateau(ActionQuantik $a, PlateauQuantik $pl, PieceQuantik $piece, int $row, int $col): string {
        //Si la pose est invalide, on désactive le bouton. L'IHM bloquera donc la pose d'une pièce invalide.
        $dis = $a->isValidePose($row, $col, $piece) ? "":"disabled";
        $valid = $a->isValidePose($row, $col, $piece) ? "valid":"";
        $ret  = "<button class='forme".$pl->getPiece($row, $col)->getForme()."".$pl->getPiece($row, $col)->getCouleur()
            ." btnCase $valid' type='submit' name='coord'";
        $ret .= "value='$row-$col'";
        $ret .= "$dis ></button>";

        return $ret;
    }

    /**
     * @param int $couleur
     * @return string
     */
    public static function getDivMessageVictoire(int $couleur) : string {
        $resultat = "<p class='victory'>Victoire des " . ($couleur == PieceQuantik::WHITE ? "Bleus" : "Oranges") . " !</p>";
        return $resultat;
    }

    /**
     * @return string
     */
    public static function getLienRecommencer():string {
        return "<p class='restartLink'><a  href=\"{$_SERVER['SCRIPT_NAME']}?reset\"> &lt;&lt;Recommencer&gt;&gt;</a></p>";
    }

    /**
     * @param array $lesPiecesDispos tableau contenant 2 ArrayPieceQuantik un pour les pièces blanches, un pour les pièces noires
     * @param int $couleurActive
     * @param PlateauQuantik $plateau
     * @return string
     */
    public static function getPageSelectionPiece(array $lesPiecesDispos, int $couleurActive, PlateauQuantik $plateau): string {
        $pageHTML = self::getDebutHTML();

        $pageHTML .= "<table id='tablePosePiece'>\n<tr>\n<td class='tdPosePiece'>";
        $pageHTML .= "<div class='divPieces'>";
        $pageHTML .= self::getFormSelectionPiece($lesPiecesDispos[$couleurActive]);
        $pageHTML .= self::getDivPiecesDisponibles($lesPiecesDispos[($couleurActive+1)%2]);
        $pageHTML .= "</div>\n</td>\n<td class='tdPosePiece'>";
        $pageHTML .= "<div class='divPlateau'>";
        $pageHTML .= self::getDivPlateauQuantik($plateau);
        $pageHTML .= "</div>\n</td>\n</tr>\n</table>\n";
        
        return $pageHTML. self::getFinHTML();
    }

    /**
     * @param array $lesPiecesDispos tableau contenant 2 ArrayPieceQuantik un pour les pièves blanches, un pour les pièces noires
     * @param int $couleurActive
     * @param int $posSelection position de la pièce sélectionnée dans la couleur active
     * @param PlateauQuantik $plateau
     * @return string
     */
    public static function getPagePosePiece(array $lesPiecesDispos, int $couleurActive, int $posSelection, PlateauQuantik $plateau): string {
        $pageHTML = self::getDebutHTML();

        $pageHTML .= "<table id='tablePosePiece'>\n<tr>\n<td class='tdPosePiece'>";
        $pageHTML .= "<div class='divPieces'>";
        $pageHTML .= self::getDivPiecesDisponibles($lesPiecesDispos[$couleurActive]);
        $pageHTML .= self::getDivPiecesDisponibles($lesPiecesDispos[($couleurActive+1)%2]);
        $pageHTML .= "</div>\n</td>\n<td class='tdPosePiece'>";
        $arr = $lesPiecesDispos[$couleurActive];
        $pageHTML .= "<div class='divPlateau'>";
        $pageHTML .= self::getFormPlateauQuantik($plateau, $arr->getPiece($posSelection), $posSelection);
        $pageHTML .= "</div>\n</td>\n</tr>\n</table>\n";
        $pageHTML .= self::getFormBoutonAnnuler();
        $pageHTML .= self::getFinHTML();

        return $pageHTML;
    }

    /**
     * @param array $lesPiecesDispos tableau contenant 2 ArrayPieceQuantik un pour les pièces blanches, un pour les pièces noires
     * @param int $couleurActive
     * @param int $posSelection
     * @param PlateauQuantik $plateau
     * @return string
     */
    public static function getPageVictoire(array $lesPiecesDispos, int $couleurActive, int $posSelection, PlateauQuantik $plateau): string {
        $pageHTML = self::getDebutHTML();

        $pageHTML .= "<table id='tablePosePiece'>\n<tr>\n<td class='tdPosePiece'>";
        $pageHTML .= "<div class='divPieces'>";
        foreach($lesPiecesDispos as $apq) {
            $pageHTML .= self::getDivPiecesDisponibles($apq);
        }
        $pageHTML .= "</div>\n</td>\n<td class='tdPosePiece'>";
        $pageHTML .= "<div class='divPlateau'>";
        $pageHTML .= self::getDivPlateauQuantik($plateau);
        $pageHTML .= "</div>\n</td>\n</tr>\n</table>\n";
        $pageHTML .= self::getDivMessageVictoire(($couleurActive+1)%2);
        $pageHTML .= self::getLienRecommencer();

        return $pageHTML . self::getFinHTML();

    }
}

