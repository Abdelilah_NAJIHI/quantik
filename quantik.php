<?php
/**
 * @author Dominique Fournier
 * @date janvier 2021
 */

use quantik\QuantikException;

require_once("PieceQuantik.php");
require_once("PlateauQuantik.php");
require_once("ActionQuantik.php");
require_once("QuantikUIGenerator.php");

echo '<link href="quantik.css" rel="quantiksheet" type="text/css">';

session_start();

if (isset($_GET['reset'])) { //pratique pour réinitialiser une partie à la main
    unset($_SESSION['etat']);
    unset($_SESSION['lesBlancs']);
    unset($_SESSION['lesNoirs']);
    unset($_SESSION['couleurActive']);
    unset($_SESSION['plateau']);
    unset($_SESSION['message']);
}

if (empty($_SESSION)) { // initialisation des variables de session
    $_SESSION['lesBlancs'] = ArrayPieceQuantik::initPiecesBlanches();
    $_SESSION['lesNoirs']  = ArrayPieceQuantik::initPiecesNoires();
    $_SESSION['plateau'] = new PlateauQuantik();
    $_SESSION['etat'] = 'choixPiece';
    $_SESSION['couleurActive'] = PieceQuantik::WHITE;
    $_SESSION['message'] = "";
}

$pageHTML = "";
$piecesDispo = array($_SESSION['lesBlancs'], $_SESSION['lesNoirs']);

$aq = new ActionQuantik($_SESSION['plateau']);
// on réalise les actions correspondant à l'action en cours :
try {
    if (isset($_GET['action'])) {
        switch ($_GET['action']) {
            case 'choisirPiece':
                $_SESSION['etat'] = "posePiece";
                break;
            case 'poserPiece':
                //Récup des coordonnées et du coin
                $row = substr($_GET['coord'],0,-2);
                $col = substr($_GET['coord'],2);
                $cor = $_SESSION['plateau']->getCornerFromCoord($row,$col);

                //Récup des pions de l'équipe active
                $arrayP = $_SESSION[$_SESSION['couleurActive'] == PieceQuantik::WHITE ? 'lesBlancs':'lesNoirs'];
                //Pose de la pièce
                $aq->posePiece($row, $col, $arrayP->getPiece($_GET['position']));
                //Suppression de la pièce posée des pièces disponibles
                $arrayP->removePieceQuantik($_GET['position']);

                //Deux cas possibles : victoire, ou on continue le jeu
                if ($aq->isRowWin($row) || $aq->isColWin($col)||$aq->isCornerWin($cor))
                    $_SESSION['etat'] = 'victoire';
                else
                    $_SESSION['etat'] = 'choixPiece';

                $_SESSION['couleurActive']++;
                $_SESSION['couleurActive'] %= 2;
                break;
            case 'annulerChoix':
                $_SESSION['etat'] = "choixPiece";
                break;
            default:
                throw new QuantikException("Action non valide");
        }
    }
} catch (QuantikException $exception) {
    $_SESSION['etat'] = 'bug';
    $_SESSION['message'] = $exception->__toString();
}

switch ($_SESSION['etat']) {
    //État de choix : on construit la page de formulaire associée au choix
    case 'choixPiece':
        $pageHTML .= QuantikUIGenerator::getPageSelectionPiece($piecesDispo,
            $_SESSION['couleurActive'],
            $_SESSION['plateau']);
        break;
    //État de pose : on construit la page de formulaire associée à la pose d'une pièce dans le jeu.
    case 'posePiece':
        $pageHTML .= QuantikUIGenerator::getPagePosePiece($piecesDispo,
            $_SESSION['couleurActive'],
            $_GET['position'][0],
            $_SESSION['plateau']);
        break;
    case 'victoire':
        $pageHTML .= QuantikUIGenerator::getPageVictoire($piecesDispo,
            $_SESSION['couleurActive'],
            $_GET['position'][0],
            $_SESSION['plateau']);
        break;
    default: // sans doute etape=bug
        echo QuantikUIGenerator::getPageErreur($_SESSION['message']);
        exit(1);
}
// seul echo nécessaire toute la pageHTML a été générée dans la variable $pageHTML
echo $pageHTML;